#include <stdio.h>
#include <stdlib.h>

double find_Diameter(double radius);
double find_Circumference(double radius);
double find_Area(double radius);

int main()
{
    float radius,area,circumference,diameter;

    printf("Please Enter the radius of a circle:");
    scanf("%f",&radius);

    diameter=find_Diameter(radius);
    circumference=find_Circumference(radius);
    area=find_Area(radius);

    printf("\n Diameter of a circle=%.2f\n",diameter);
    printf("Circumference of a circle=%.2f\n",circumference);
    printf("Area of a Circle=%.2f\n",area);

    return 0;
}
     double find_Diameter(double radius)
{
      return 2*radius;
}
    double find_Circumference(double radius)

{

    return 2*3.14*radius;
}
    double find_Area(double radius)
{
    return 3.14*radius*radius;

}
